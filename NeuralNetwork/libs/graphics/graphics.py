'''
Created on 21-Jan-2019

@author: Me
'''

import turtle;

class Graphics(object):
    '''
    Simple graphics library with Turtle
    '''
    window = None;
    pointer = None;

    def __init__(self, bgcolor="light green", title="Window Title", screen_width=300,screen_height=300):
        '''
        Constructor
        '''
        
        self.window = turtle.Screen()
        self.changebgcolor(bgcolor);
        self.window.title(title)
        self.pointer = turtle.Turtle() 
#         self.pointer.screensize(screen_width, screen_height)
        
    def changebgcolor(self, bgcolor):
        self.window.bgcolor(bgcolor);
    
    def clearscreen(self):
        self.window.clearscreen();
        
    def exitonclick(self):
        self.window.exitonclick();
    
    def changepointercolor(self, color):
        self.pointer.pencolor(color);
    
    def drawline(self, topoint, frompoint=(0,0)):
        self.pointer.penup();
        self.pointer.goto(frompoint[0], frompoint[1]);
        self.pointer.pendown();
        self.pointer.goto(topoint[0], topoint[1]);
    
    def changepensize(self,width=2):
        self.pointer.pensize(width)
    
    def changepencolor(self,color="black"):
        self.pointer.pencolor(color);
    
    def drawcircle(self, position=(0,0), radius=5):
        self.pointer.penup();
        self.pointer.goto(position[0], position[1]);
        self.pointer.pendown();
        self.pointer.circle(radius);
    
g = Graphics(title="Turtle");
g.changepencolor("red");
g.changepensize(5)
g.drawcircle((0,0), 10);
g.exitonclick()